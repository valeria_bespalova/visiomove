VisioMove Essential for iOS
====================

This framework allows you to integrate VisioMove Essential into your iOS app.

Learn more about the provided samples, documentation, integrating the SDK into
your app, and more within the provided documentation.

LICENSE
-------
See the LICENSE file.

SAMPLES
-------
To see VisioMove Essential in action, check out the provided samples.  Currently,
there is a MapViewSample project that lives in the "samples" directory.

