var interface_v_m_e_route_request =
[
    [ "addDestination:", "interface_v_m_e_route_request.html#a87cd00a5edc681a5754be744c62d612d", null ],
    [ "addDestinations:", "interface_v_m_e_route_request.html#a845a4c97bb8004c82446473bf7445fdf", null ],
    [ "getDestinations", "interface_v_m_e_route_request.html#a7d050d533b428bdd51ee041c867dcfeb", null ],
    [ "getOrigin", "interface_v_m_e_route_request.html#a106d1bd2cbcdc8b1654e5db7a961322b", null ],
    [ "initWithAccessible:optimize:", "interface_v_m_e_route_request.html#a7ba3ab25a4a9a2ff2640087adfac8cbb", null ],
    [ "initWithRequestType:destinationsOrder:", "interface_v_m_e_route_request.html#a639a7239b6fbdcf1311baf509ee3fea4", null ],
    [ "initWithRequestType:destinationsOrder:accessible:", "interface_v_m_e_route_request.html#a4d0359a78f6b28fb5e7fe7be15051d4f", null ],
    [ "isEqualToRouteRequest:", "interface_v_m_e_route_request.html#a50aa7ce2dff3af0c17ef1b1aced9fd1a", null ],
    [ "removeAllDestinations", "interface_v_m_e_route_request.html#a349b38501cecad898a541f82c2098e9f", null ],
    [ "removeDestinationAtIndex:", "interface_v_m_e_route_request.html#a51a96105e0af6c9500160d5627544421", null ],
    [ "replaceDestinationAtIndex:withDestination:", "interface_v_m_e_route_request.html#a2516338cd8e1b3bbad03b20676bec333", null ],
    [ "setOrigin:", "interface_v_m_e_route_request.html#aab8bcd0f004e2e2596f48ad82c895591", null ],
    [ "setOriginWithLocation:", "interface_v_m_e_route_request.html#a45a75136506aa90354622620d23c53c1", null ],
    [ "setOriginWithPlaceID:", "interface_v_m_e_route_request.html#a98660904b47f0fb73afbb18673b38933", null ],
    [ "destinationsOrder", "interface_v_m_e_route_request.html#af1d8f7fd8be089a876bba9e7740c49b9", null ],
    [ "isAccessible", "interface_v_m_e_route_request.html#a5f6712810ba9266ec2ffbc6e16710970", null ],
    [ "isOptimal", "interface_v_m_e_route_request.html#a8a500caf9f98f5d6cd85d808b3135fe1", null ],
    [ "requestType", "interface_v_m_e_route_request.html#ac812f22f09b8f3178d5364475f0ec143", null ]
];