var protocol_v_m_e_overlay_view_interface_p =
[
    [ "addOverlayViewID:view:placeID:", "protocol_v_m_e_overlay_view_interface-p.html#a9eeb4ff7c14738d3ce1c855194b7cc37", null ],
    [ "addOverlayViewID:view:position:", "protocol_v_m_e_overlay_view_interface-p.html#a9ee6c55fe60032d19563ee788c9711b9", null ],
    [ "removeOverlayViewID:", "protocol_v_m_e_overlay_view_interface-p.html#a5f6fb56e4f007f9fea8f592741e687c9", null ],
    [ "setOverlayViewID:placeID:", "protocol_v_m_e_overlay_view_interface-p.html#acfafbb3ce43d621ac3b1ec7313f6fe77", null ],
    [ "setOverlayViewID:position:", "protocol_v_m_e_overlay_view_interface-p.html#a2aa31390ac31f9ee983cdf8d34ccfeb4", null ]
];