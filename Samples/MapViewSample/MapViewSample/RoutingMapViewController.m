/*
 * Copyright 2016, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */


#import "RoutingMapViewController.h"

@implementation RoutingMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = NSLocalizedString(@"RoutingMapViewController.title", nil);
    self.enableAccessibleRouteLabel.text = NSLocalizedString(@"RoutingMapViewController.accessible", nil);
    self.enableOptmizeLabel.text = NSLocalizedString(@"RoutingMapViewController.optimize", nil);
    [self.computeRoute setTitle:NSLocalizedString(@"RoutingMapViewController.createRouteFromPlace", nil)
                       forState:UIControlStateNormal];
    [self.computeRouteFromLocation setTitle:NSLocalizedString(@"RoutingMapViewController.createRouteFromLocation", nil)
                                   forState:UIControlStateNormal];
    
    [self.mapView setMapListener:self];
    [self.mapView loadMap];
    
    // IMPORTANT: Ensure the mapView starts below the navigation bar, otherwise
    // VisioMove Essential's search view button will be hidden.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (IBAction)computeRouteFromPlace:(id)sender
{
    BOOL lIsAccessible = [self.enableAccessibleRouteSwitch isOn];
    VMERouteDestinationsOrder lDestOrder = [self.enableOptimizeSwitch isOn] ? VMERouteDestinationsOrderOptimal : VMERouteDestinationsOrderInOrder;
    VMERouteRequest* lRouteRequest = [[VMERouteRequest alloc] initWithRequestType:VMERouteRequestTypeFastest
                                                                destinationsOrder:lDestOrder
                                                                       accessible:lIsAccessible];
    [lRouteRequest setOrigin:@"Pp23"];
    [lRouteRequest addDestinations:@[@"0022s", @"0040s", @"8090"]];
    
    [self.mapView computeRoute:lRouteRequest callback:self];
}

- (IBAction)computeRouteFromLocation:(id)sender {
    
    BOOL lIsAccessible = [self.enableAccessibleRouteSwitch isOn];
    VMERouteDestinationsOrder lDestOrder = [self.enableOptimizeSwitch isOn] ? VMERouteDestinationsOrderOptimal : VMERouteDestinationsOrderInOrder;
    VMERouteRequest* lRouteRequest = [[VMERouteRequest alloc] initWithRequestType:VMERouteRequestTypeFastest
                                                                destinationsOrder:lDestOrder
                                                                       accessible:lIsAccessible];
    // The altitude determines the layer the position is associated with
    CLLocation* lCLLocation = [[CLLocation alloc] initWithCoordinate:TEST_COORDINATE
                                                            altitude:0
                                                  horizontalAccuracy:5
                                                    verticalAccuracy:0
                                                              course:-1
                                                               speed:5
                                                           timestamp:[NSDate date]];
    VMEPosition* lPositionStart = [self.mapView createPositionFromLocation:lCLLocation];
    
    [lRouteRequest setOrigin:lPositionStart];
    
    lCLLocation = [[CLLocation alloc] initWithCoordinate:TEST_COORDINATE_PLUS_DELTA
                                                altitude:0
                                      horizontalAccuracy:5
                                        verticalAccuracy:0
                                                  course:-1
                                                   speed:5
                                               timestamp:[NSDate date]];
    VMEPosition* lPositionWaypoint = [self.mapView createPositionFromLocation:lCLLocation];
    
    [lRouteRequest addDestinations:@[lPositionWaypoint, @"8030", @"8066"]];
    
    [self.mapView computeRoute:lRouteRequest callback:self];
}

#pragma mark - VMEMapListener

-(void) mapDidLoad:(VMEMapView*)mapView
{
    [self computeRouteFromPlace:nil];
}


#pragma mark - VMEComputeRouteCallback
-(BOOL) computeRouteDidFinish:(VMEMapView*)mapView
                   parameters:(VMERouteRequest *)routeRequest
                       result:(VMERouteResult *)routeResult

{
    NSLog(@"computeRouteDidFinish duration: %.0fmins and length: %.0fm ", (routeResult.duration / 60), routeResult.length);
    return YES;
}


-(void) computeRouteDidFail:(VMEMapView*)mapView
                 parameters:(VMERouteRequest*)routeRequest
                      error:(NSString*)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Route failed"
                                                    message:error
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
