/*
 * Copyright 2016, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */


#import "MapViewController.h"

@interface MapViewController ()
@property (nonatomic) BOOL mapReady;
@property (nonatomic, copy) void(^mOnMapLoadedBlock)(MapViewController* pMapViewController);
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.mapReady = NO;
    self.title = @"Map";
    
    [self.mapView setMapListener:self];
    [self.mapView loadMap];
    
    // Ensure that the map view is below the navigation bar.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) performMapAction:(void(^)(MapViewController* pMyViewController)) block
{
    if (self.mapReady)
    {
        block(self);
    }
    else
    {
        self.mOnMapLoadedBlock = block;
    }
}


#pragma mark - VMEMapListener

-(void) mapDidLoad:(VMEMapView*)mapView
{
    self.mapReady = YES;
    if (self.mOnMapLoadedBlock)
    {
        void (^lBlock)(MapViewController*) = self.mOnMapLoadedBlock;
        self.mOnMapLoadedBlock = nil;
        __weak MapViewController* weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            MapViewController* strongSelf = weakSelf;
            if (strongSelf)
            {
                lBlock(strongSelf);
            }
        });
    }
}

-(void) mapReadyForPlaceUpdate:(VMEMapView *)mapView
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"visio_island_cms_update" ofType:@"json"];
    NSData *returnedData = [[NSFileManager defaultManager] contentsAtPath:path];
    
    if(NSClassFromString(@"NSJSONSerialization"))
    {
        NSError *error = nil;
        id object = [NSJSONSerialization
                     JSONObjectWithData:returnedData
                     options:0
                     error:&error];

        if(error == nil
           && [object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *results = object;
            [mapView updatePlaceData:[results valueForKeyPath:@"locale.en"]];
        }
    }
}

@end
