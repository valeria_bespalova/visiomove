/*
 * Copyright 2016, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */


#import "LocationMapViewController.h"

@interface LocationMapViewController ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation LocationMapViewController

-(void)dealloc
{
    if (_locationManager) {
        _locationManager.delegate = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = NSLocalizedString(@"LocationMapViewController.title", nil);
    self.enableLocationLabel.text = NSLocalizedString(@"LocationMapViewController.location", nil);
    [self.simulateLocationButton setTitle:NSLocalizedString(@"LocationMapViewController.simulateLocation", nil) forState:UIControlStateNormal];
    
    [self.mapView loadMap];
    
    // IMPORTANT: Ensure the mapView starts below the navigation bar, otherwise
    // VisioMove Essential's search view button will be hidden.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}


- (IBAction)injectSinglePosition:(id)sender {    
    CLLocation* lCLLocation = [[CLLocation alloc] initWithCoordinate:TEST_COORDINATE
                                                            altitude:0
                                                  horizontalAccuracy:5
                                                    verticalAccuracy:0
                                                              course:90
                                                               speed:5
                                                           timestamp:[NSDate date]];
    
    VMELocation* lVMELocation = [self.mapView createLocationFromLocation:lCLLocation];
    
    [self.mapView updateLocation:lVMELocation];
}

- (IBAction)enableLocation:(id)sender {
    
    if(!self.locationManager)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        // This is required in iOS8 in order to prompt the user to accept
        // to use the localisation
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    
    if ([sender isOn])
    {
        [self.locationManager startUpdatingLocation];
    }
    else
    {
        [self.locationManager stopUpdatingLocation];
        [self.mapView updateLocation:nil];
    }
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* lCLLocation = locations.lastObject;
    VMELocation* lVMELocation = [self.mapView createLocationFromLocation:lCLLocation];
    [self.mapView updateLocation:lVMELocation];
}

@end
