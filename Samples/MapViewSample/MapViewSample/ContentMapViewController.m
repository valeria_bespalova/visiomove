/*
 * Copyright 2016, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */
#import "ContentMapViewController.h"

@implementation ContentMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"ContentMapViewController.title", nil);
    self.placeColorLabel.text = NSLocalizedString(@"ContentMapViewController.placeColorLabel", nil);
    self.placeAddLabel.text = NSLocalizedString(@"ContentMapViewController.placeAddLabel", nil);
    
    
    self.mapView.mapListener = self;
    [self.mapView loadMap];
    
    // IMPORTANT: Ensure the mapView starts below the navigation bar, otherwise
    // VisioMove Essential's search view button will be hidden.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}


- (IBAction)placeColorSwitchAction:(id)sender
{
    NSArray* lPlaceIDs = [[NSArray alloc] initWithObjects:@"0040s",
                          @"0006",
                          @"0012",
                          @"8030",
                          nil];
    
    UISwitch* lSwitch = (UISwitch*)sender;
    if (lSwitch.isOn)
    {
        NSInteger aRedValue = arc4random()%255;
        NSInteger aGreenValue = arc4random()%255;
        NSInteger aBlueValue = arc4random()%255;
        UIColor *randColor = [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1.0f];
        
        for (NSString* lPlaceID in lPlaceIDs)
        {
            [self.mapView setPlaceID:lPlaceID color:randColor];
        }
    }
    else
    {
        for (NSString* lPlaceID in lPlaceIDs)
        {
            [self.mapView resetPlaceIDColor:lPlaceID];
        }
    }
    
    CGFloat padding = 100;
    
    // Move the camera to look at the color change
    VMECameraUpdate* lUpdate = [VMECameraUpdate cameraUpdateForPlaceID:lPlaceIDs.firstObject
                                                               heading:[VMECameraHeading cameraHeadingCurrent]
                                                            paddingTop:padding
                                                         paddingBottom:padding
                                                           paddingLeft:padding
                                                          paddingRight:padding];
    [self.mapView animateCamera:lUpdate];
}

- (IBAction)placeAddSwitchAction:(id)sender {
    UISwitch* lSwitch = (UISwitch*)sender;
    NSString* lCatOfflineMainBundle = @"lCatOfflineMainBundle";
    NSString* lCatMapBundle = @"lCatMapBundle";
    NSString* lCatOnline = @"lCatOnline";
    
    if (lSwitch.isOn)
    {
        NSMutableArray* lPositions = [[NSMutableArray alloc] init];
        
        // lCatMapBundle
        {
            VMEPosition* lPos = [[VMEPosition alloc] initWithLatitude:TEST_COORDINATE_LAT - DELTA
                                                            longitude:TEST_COORDINATE_LON + DELTA
                                                             altitude:0.0
                                                           buildingID:nil
                                                              floorID:nil];
            [lPositions addObject:lPos];
            
            NSURL *lIconUrl = [NSURL URLWithString:@"https://images.pexels.com/photos/45201/kitty-cat-kitten-pet-45201.jpeg?h=350&auto=compress&cs=tinysrgb"];
            NSDictionary *placeData = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"Cat 1", @"name",
                                       @"<b>Bold Text</b>. Cras ultricies ligula sed magna dictum porta", @"description",
                                       lIconUrl.absoluteString, @"icon",
                                       @[@"2", @"3", @"99"], @"categories",
                                       nil];
            
            [self.mapView addPlaceID:lCatMapBundle
                            imageURL:lIconUrl
                                data:placeData
                            position:lPos
                                size:[[VMEPlaceSize alloc] initWithScale:1.0]
                          anchorMode:VMEPlaceAnchorModeBottomCenter
                        altitudeMode:VMEPlaceAltitudeModeRelative
                         displayMode:VMEPlaceDisplayModeOverlay
                         orientation:[VMEPlaceOrientation placeOrientationFacing]
                      visibilityRamp:[[VMEPlaceVisibilityRamp alloc] init]
             ];
            [self.mapView setPlaceID:lCatMapBundle size:[[VMEPlaceSize alloc] initWithScale:20.0] animated:YES];
        }
        
        
        // lCatOfflineAsset
        {
            VMEPosition* lPos = [[VMEPosition alloc] initWithLatitude:TEST_COORDINATE_LAT - DELTA
                                                            longitude:TEST_COORDINATE_LON - DELTA
                                                             altitude:0.0
                                                           buildingID:nil
                                                              floorID:nil];
            [lPositions addObject:lPos];
            
            NSString* lFilePath = [[NSBundle mainBundle] pathForResource:@"marker_cat_curious" ofType:@"png"];
            NSURL *lIconUrl = [NSURL URLWithString:lFilePath];
            NSDictionary *placeData = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"Cat - Main bundle", @"name",
                                       @"<b>Sed</b> porttitor lectus nibh", @"description",
                                       lIconUrl.absoluteString, @"icon",
                                       @[@"2", @"3", @"99"], @"categories",
                                       nil];
            
            [self.mapView addPlaceID:lCatOfflineMainBundle
                            imageURL:lIconUrl
                                data:placeData
                            position:lPos
                                size:[[VMEPlaceSize alloc] initWithScale:1.0]
                          anchorMode:VMEPlaceAnchorModeBottomCenter
                        altitudeMode:VMEPlaceAltitudeModeRelative
                         displayMode:VMEPlaceDisplayModeOverlay
                         orientation:[VMEPlaceOrientation placeOrientationFacing]
                      visibilityRamp:[[VMEPlaceVisibilityRamp alloc] init]
             ];
            [self.mapView setPlaceID:lCatOfflineMainBundle size:[[VMEPlaceSize alloc] initWithScale:20.0] animated:YES];
        }
        
        // lCatOnline
        {
            VMEPosition* lPos = [[VMEPosition alloc] initWithLatitude:TEST_COORDINATE_LAT + DELTA
                                                            longitude:TEST_COORDINATE_LON + DELTA
                                                             altitude:0.0
                                                           buildingID:nil
                                                              floorID:nil];
            [lPositions addObject:lPos];
            
            NSURL *lIconUrl = [NSURL URLWithString:@"http://www.clipartbest.com/cliparts/aTe/K4e/aTeK4en8c.png"];
            
            NSDictionary *placeData = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"Cat - Online", @"name",
                                       @"https://en.wikipedia.org/wiki/Cat", @"description",
                                       lIconUrl.absoluteString, @"icon",
                                       @[@"2", @"3", @"99"], @"categories",
                                       nil];
            
            [self.mapView addPlaceID:lCatOnline
                            imageURL:lIconUrl
                                data:placeData
                            position:lPos
                                size:[[VMEPlaceSize alloc] initWithScale:1.0]
                          anchorMode:VMEPlaceAnchorModeBottomCenter
                        altitudeMode:VMEPlaceAltitudeModeRelative
                         displayMode:VMEPlaceDisplayModeOverlay
                         orientation:[VMEPlaceOrientation placeOrientationFacing]
                      visibilityRamp:[[VMEPlaceVisibilityRamp alloc] init]
             ];
            [self.mapView setPlaceID:lCatOnline size:[[VMEPlaceSize alloc] initWithScale:20.0] animated:YES];
        }
        
        CGFloat lPadding = 50;
        VMECameraUpdate* lCameraUpdate = [VMECameraUpdate cameraUpdateForPositions:lPositions
                                                                           heading:[VMECameraHeading cameraHeadingCurrent]
                                                                        paddingTop:lPadding
                                                                     paddingBottom:lPadding
                                                                       paddingLeft:lPadding
                                                                      paddingRight:lPadding];
        [self.mapView animateCamera:lCameraUpdate];
    }
    else
    {
        [self.mapView removePlaceID:lCatMapBundle];
        [self.mapView removePlaceID:lCatOfflineMainBundle];
        [self.mapView removePlaceID:lCatOnline];
    }
}

#pragma mark - VMEMapListener

-(void) mapDidLoad:(VMEMapView*)mapView
{}

/**
 * VisioMove Essential calls the below method while it's loading the map. We
 * can update the place data that appears within the map here.  
 *
 * In this example we read the place data from a local json file.  In reality, 
 * the data would most likely be downloaded from a remote webservice to ensure
 * the most up to date information.
 */
-(void) mapReadyForPlaceUpdate:(VMEMapView *)mapView
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"visio_island_cms_update" ofType:@"json"];
    NSData *returnedData = [[NSFileManager defaultManager] contentsAtPath:path];
    
    if(NSClassFromString(@"NSJSONSerialization"))
    {
        NSError *error = nil;
        id object = [NSJSONSerialization
                     JSONObjectWithData:returnedData
                     options:0
                     error:&error];
        
        if(error == nil
           && [object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *results = object;
            [self.mapView updatePlaceData:[results valueForKeyPath:@"locale.en"]];
        }
    }
}

@end
