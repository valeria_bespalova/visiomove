//
//  OverlayViewController.m
//  MapViewSample
//
//  Created by Jon Bryant on 10/06/2016.
//  Copyright © 2016 Visioglobe. All rights reserved.
//

#import "OverlayViewController.h"

@interface VMEArrowView : UIView
@property (nonatomic, strong) UILabel* title;
@property (nonatomic, strong) UIButton* close;
@end

@implementation VMEArrowView

#define kArrowHeight 15

-(instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 20)];
        _title.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_title];
        _close = [UIButton buttonWithType:UIButtonTypeCustom];
        _close.frame = CGRectMake(0, 20, frame.size.width, 20);
        [self addSubview:_close];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIBezierPath *fillPath = [UIBezierPath bezierPath];
    [fillPath moveToPoint:CGPointMake(0, 0)];
    [fillPath addLineToPoint:CGPointMake(self.bounds.size.width, 0)];
    [fillPath addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height-kArrowHeight)];
    [fillPath addLineToPoint:CGPointMake(self.bounds.size.width/2+(kArrowHeight/2), self.bounds.size.height-kArrowHeight)];
    [fillPath addLineToPoint:CGPointMake(self.bounds.size.width/2, self.bounds.size.height)];
    [fillPath addLineToPoint:CGPointMake(self.bounds.size.width/2-(kArrowHeight/2), self.bounds.size.height-kArrowHeight)];
    [fillPath addLineToPoint:CGPointMake(0, self.bounds.size.height-kArrowHeight)];
    [fillPath closePath];
    
    CGContextAddPath(context, fillPath.CGPath);
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextFillPath(context);
    
    CAShapeLayer *shape = [CAShapeLayer layer];
    shape.path = fillPath.CGPath;
    shape.lineWidth = 2;
    shape.strokeColor = [UIColor grayColor].CGColor;
    shape.fillColor = [UIColor clearColor].CGColor;
    shape.frame = self.bounds;
    [self.layer addSublayer:shape];
    
}

@end


static NSString* sLonePineOverlayViewID = @"lonePineOverlayView";
static NSString* sPlaceOverlayViewID = @"placeOverlayView";

@interface OverlayViewController ()
@property (strong, nonatomic) VMEArrowView* mLonePineView;

@property (strong, nonatomic) NSMutableDictionary* mOverlays;
@property (nonatomic) BOOL mIsPlaceMarkerDisplayed;
@end

@implementation OverlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"OverlayViewController.title", nil);
    self.infoViewLabel.text = NSLocalizedString(@"OverlayViewController.toggleOverlays", nil);
    [self.moveOverlayButton setTitle:NSLocalizedString(@"OverlayViewController.moveOverlays", nil) forState:UIControlStateNormal];
    self.moveOverlayButton.enabled = NO;
    
    // Must be called in order for the map to be loaded
    [self.mapView loadMap];
    
    self.mOverlays = [[NSMutableDictionary alloc] init];
    [self.mOverlays setObject:[[VMEArrowView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)] forKey:@"Overlay1"];
    [self.mOverlays setObject:[[VMEArrowView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)] forKey:@"Overlay2"];
    [self.mOverlays setObject:[[VMEArrowView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)] forKey:@"Overlay3"];
    [self.mOverlays setObject:[[VMEArrowView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)] forKey:@"Overlay4"];
    [self.mOverlays setObject:[[VMEArrowView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)] forKey:@"Overlay5"];
    
    self.mIsPlaceMarkerDisplayed = NO;
    
    // IMPORTANT: Ensure the mapView starts below the navigation bar, otherwise
    // VisioMove Essential's search view button will be hidden.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}


- (IBAction)infoViewSwitch:(id)sender
{
    UISwitch* lSwitch = (UISwitch*) sender;
    if([lSwitch isOn])
    {
        VMEArrowView* lView;
        
        // overlay on outside place
        lView = [self.mOverlays objectForKey:@"Overlay1"];
        lView.title.text = @"Lone Pine Overlay View";
        lView.close.hidden = YES;
        [self.mapView addOverlayViewID:@"Overlay1" view:lView placeID:@"outside-lonepine"];
        
        NSString *placeID = @"0012";
        // overlay on indoor place
        lView = [self.mOverlays objectForKey:@"Overlay2"];
        lView.title.text = placeID;
        lView.close.hidden = YES;
        [self.mapView addOverlayViewID:@"Overlay2" view:lView placeID:placeID];
        
        placeID = @"0036";
        // overlay on underground place
        lView = [self.mOverlays objectForKey:@"Overlay3"];
        lView.title.text = placeID;
        lView.close.hidden = YES;
        [self.mapView addOverlayViewID:@"Overlay3" view:lView placeID:placeID];
        
        CLLocationCoordinate2D coordinate = RANDOM_COORDINATE;
        // One with lat/lon pos in the first floor of the offices
        lView = [self.mOverlays objectForKey:@"Overlay4"];
        lView.title.text =  [NSString stringWithFormat:@"%@ N %@ E", @(coordinate.latitude), @(coordinate.longitude)];
        lView.close.hidden = YES;
        
        VMEPosition* lPos = [[VMEPosition alloc] initWithLatitude:coordinate.latitude
                                                        longitude:coordinate.longitude
                                                         altitude:0
                                                       buildingID:BUILDING
                                                          floorID:FLOOR_1];
        
        [self.mapView addOverlayViewID:@"Overlay4" view:lView position:lPos];
        
        
        self.moveOverlayButton.enabled = YES;
    }
    else
    {
        for (NSString* lKey in self.mOverlays)
        {
            [self.mapView removeOverlayViewID:lKey];
        }
        self.moveOverlayButton.enabled = NO;
    }
}

- (IBAction)moveOverlay:(id)sender {
    CLLocationCoordinate2D coordinate = RANDOM_COORDINATE;
    VMEPosition* lPosOutside = [[VMEPosition alloc] initWithLatitude:coordinate.latitude
                                                           longitude:coordinate.longitude
                                                            altitude:0
                                                          buildingID:nil
                                                             floorID:nil];
    
    [self.mapView setOverlayViewID:@"Overlay4" position:lPosOutside];
}

-(IBAction) closeMarkerPlace:(id)sender
{
    [self.mapView removeOverlayViewID:sPlaceOverlayViewID];
    self.mIsPlaceMarkerDisplayed = NO;
}

#pragma mark - VMEMapListener
-(BOOL) map:(VMEMapView*)mapView didSelectPlace:(NSString*)placeID withPosition:(VMEPosition*)position
{
    if(self.mIsPlaceMarkerDisplayed)
    {
        [self.mapView setOverlayViewID:sPlaceOverlayViewID position:position];
    }
    else
    {
        VMEArrowView* lView = [[VMEArrowView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)];
        [lView.title setText:placeID];
        [lView.close addTarget:self action:@selector(closeMarkerPlace:) forControlEvents:UIControlEventTouchUpInside];
        [lView.close setTitle:@"Close" forState:UIControlStateNormal];
        
        
        [self.mapView addOverlayViewID:sPlaceOverlayViewID view:lView position:position];
        self.mIsPlaceMarkerDisplayed = YES;
    }
    
    NSArray* lPositions = [[NSArray alloc] initWithObjects:position, nil];
    VMECameraUpdate* lUpdate = [VMECameraUpdate cameraUpdateForPositions:lPositions
                                                                 heading:[VMECameraHeading cameraHeadingCurrent]
                                                              paddingTop:100
                                                           paddingBottom:0
                                                             paddingLeft:0
                                                            paddingRight:0];
    [self.mapView animateCamera:lUpdate];
    
    return YES;
}


@end
