var interface_v_m_e_position =
[
    [ "initWithLatitude:longitude:altitude:", "interface_v_m_e_position.html#a7fd51e30c080834173b14cf4f8ede675", null ],
    [ "initWithLatitude:longitude:altitude:buildingID:floorID:", "interface_v_m_e_position.html#a1907c22f0cdc0eca1c5f524a3deefe62", null ],
    [ "isEqualToPosition:", "interface_v_m_e_position.html#a3acca457eac8b6c42b6f15b644db3ebe", null ],
    [ "altitude", "interface_v_m_e_position.html#ae37b06d135f3fc086885094b860ad57b", null ],
    [ "buildingID", "interface_v_m_e_position.html#ad36ee3897d34abcf702e40c4383349c6", null ],
    [ "floorID", "interface_v_m_e_position.html#a09691e339c0bbb203d8d49bd47d90665", null ],
    [ "latitude", "interface_v_m_e_position.html#af1c1139d247e495d24eee0b0beb9d628", null ],
    [ "longitude", "interface_v_m_e_position.html#a352fb16966419a8851c4843d770925ff", null ]
];