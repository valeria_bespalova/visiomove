/*
 * Copyright 2016, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */

// Basic map
"BasicMapViewController.title" = "Basic Map";
"BasicMapViewController.description" = "Launches a map and also demos a different color theme.";

// Camera
"CameraMapViewController.title" = "Camera";
"CameraMapViewController.description" = "Demonstrates camera functions";
"CameraMapViewController.animate" = "Animate";
"CameraMapViewController.floor" = "Floor";
"CameraMapViewController.building" = "Building";
"CameraMapViewController.global" = "Global";
"CameraMapViewController.place" = "Place";
"CameraMapViewController.position" = "Position";

// Routing
"RoutingMapViewController.title" = "Routing";
"RoutingMapViewController.description" = "Demonstrates how to create routes within the map";
"RoutingMapViewController.accessible" = "Accessible";
"RoutingMapViewController.optimize" = "Optimize";
"RoutingMapViewController.createRouteFromPlace" = "Route from place";
"RoutingMapViewController.createRouteFromLocation" = "Route from location";

// Location
"LocationMapViewController.title" = "Location";
"LocationMapViewController.description" = "Demonstrates how to connect location services";
"LocationMapViewController.location" = "Location Services";
"LocationMapViewController.simulateLocation" = "Simulate Location";

// Content Management
"ContentMapViewController.title" = "Content Management";
"ContentMapViewController.description" = "Demonstrates updating place data during initialisation";
"ContentMapViewController.placeColorLabel" = "Toggle place color";
"ContentMapViewController.placeAddLabel" = "Cat tracker";

// Search
"SearchMapViewController.title" = "Search";
"SearchMapViewController.description" = "Demonstrates how to open the search view";
"SearchMapViewController.searchButton" = "Open search";
"SearchMapViewController.gotoButton" = "Visit";
"SearchMapViewController.nothingSelected" = "Nothing selected";

// Overlay View
"OverlayViewController.title" = "Overlay View";
"OverlayViewController.description" = "Demonstrates how overlay views can be added to the map";
"OverlayViewController.toggleOverlays" = "Toggle overlays";
"OverlayViewController.moveOverlays" = "Move overlays";
