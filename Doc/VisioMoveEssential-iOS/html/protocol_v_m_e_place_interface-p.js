var protocol_v_m_e_place_interface_p =
[
    [ "addPlaceID:imageURL:data:position:", "protocol_v_m_e_place_interface-p.html#aa20c87c632415e31ff1807b84579341d", null ],
    [ "addPlaceID:imageURL:data:position:size:anchorMode:altitudeMode:displayMode:orientation:visibilityRamp:", "protocol_v_m_e_place_interface-p.html#a7c50a3f9acf6649f3684f65af989c196", null ],
    [ "queryAllPlaceIDs", "protocol_v_m_e_place_interface-p.html#af6011c5060475bcb6d4bc1e29fa14750", null ],
    [ "removePlaceID:", "protocol_v_m_e_place_interface-p.html#a024bb2a6cbf87d86ad2b4ed100729852", null ],
    [ "resetPlaceIDColor:", "protocol_v_m_e_place_interface-p.html#a269b24dd18b882b52dde4d14d812e8a4", null ],
    [ "setPlaceID:color:", "protocol_v_m_e_place_interface-p.html#a9b729833964073151d170bc6b8d6a8b9", null ],
    [ "setPlaceID:data:", "protocol_v_m_e_place_interface-p.html#a517a642549f1fbd44a13b9b31141c998", null ],
    [ "setPlaceID:position:animated:", "protocol_v_m_e_place_interface-p.html#aafae5583ebb113b89e8d4d41b82ab8f6", null ],
    [ "setPlaceID:size:animated:", "protocol_v_m_e_place_interface-p.html#a18b1ac969d810158004689dc7c4c8119", null ],
    [ "updatePlaceData:", "protocol_v_m_e_place_interface-p.html#a00a70eec8be29f86d8ac5a796144c085", null ]
];