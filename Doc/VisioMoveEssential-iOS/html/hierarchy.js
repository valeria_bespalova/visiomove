var hierarchy =
[
    [ "<NSCopying>", null, [
      [ "VMELocation", "interface_v_m_e_location.html", null ],
      [ "VMEPosition", "interface_v_m_e_position.html", null ],
      [ "VMERouteRequest", "interface_v_m_e_route_request.html", null ],
      [ "VMERouteResult", "interface_v_m_e_route_result.html", null ]
    ] ],
    [ "NSObject", null, [
      [ "VMECameraHeading", "interface_v_m_e_camera_heading.html", null ],
      [ "VMECameraUpdate", "interface_v_m_e_camera_update.html", null ],
      [ "VMELocation", "interface_v_m_e_location.html", null ],
      [ "VMEPlaceOrientation", "interface_v_m_e_place_orientation.html", null ],
      [ "VMEPlaceSize", "interface_v_m_e_place_size.html", null ],
      [ "VMEPlaceVisibilityRamp", "interface_v_m_e_place_visibility_ramp.html", null ],
      [ "VMEPosition", "interface_v_m_e_position.html", null ],
      [ "VMERouteRequest", "interface_v_m_e_route_request.html", null ],
      [ "VMERouteResult", "interface_v_m_e_route_result.html", null ],
      [ "VMESceneUpdate", "interface_v_m_e_scene_update.html", null ]
    ] ],
    [ "<NSObject>", null, [
      [ "<VMEComputeRouteCallback>", "protocol_v_m_e_compute_route_callback-p.html", null ],
      [ "<VMEComputeRouteInterface>", "protocol_v_m_e_compute_route_interface-p.html", null ],
      [ "<VMELocationInterface>", "protocol_v_m_e_location_interface-p.html", null ],
      [ "<VMEMapListener>", "protocol_v_m_e_map_listener-p.html", null ],
      [ "<VMEOverlayViewInterface>", "protocol_v_m_e_overlay_view_interface-p.html", null ],
      [ "<VMESearchViewCallback>", "protocol_v_m_e_search_view_callback-p.html", null ],
      [ "<VMESearchViewInterface>", "protocol_v_m_e_search_view_interface-p.html", null ]
    ] ],
    [ "<NSObjectNSObject>", null, [
      [ "<VMEMapInterface>", "protocol_v_m_e_map_interface-p.html", null ],
      [ "<VMEPlaceInterface>", "protocol_v_m_e_place_interface-p.html", null ]
    ] ],
    [ "UIView", null, [
      [ "VMEMapView", "interface_v_m_e_map_view.html", null ]
    ] ]
];