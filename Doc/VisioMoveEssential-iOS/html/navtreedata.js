var NAVTREE =
[
  [ "VisioMove Essential (iOS)", "index.html", [
    [ "Getting started", "index.html", null ],
    [ "Change Log", "changeLog.html", [
      [ "v1.4 - July 12, 2017", "changeLog.html#v140", null ],
      [ "v1.3 - April 24, 2017", "changeLog.html#v130", null ],
      [ "v1.2.1 - February 24, 2017", "changeLog.html#v121", null ],
      [ "v1.2 - February 17, 2017", "changeLog.html#v120", null ],
      [ "v1.1.2 - December 9, 2016", "changeLog.html#v112", null ],
      [ "v1.1.1 - October 13, 2016", "changeLog.html#v111", null ],
      [ "v1.1 - August 24, 2016", "changeLog.html#v110", null ],
      [ "v1.0 - May 13, 2016", "changeLog.html#v100", null ],
      [ "v0.0.3b - March 29, 2016", "changeLog.html#v003b", null ]
    ] ],
    [ "FAQ", "faq.html", [
      [ "Localizing the SDK", "faq.html#localeApp", null ],
      [ "Localizing the venue", "faq.html#localeMapVenue", null ],
      [ "Localizing place data", "faq.html#place", null ],
      [ "Place data format", "faq.html#placeDataFormat", null ],
      [ "Place and category icon paths", "faq.html#iconPath", null ],
      [ "iOS 9 compatibility", "faq.html#iosNine", null ],
      [ "Changing resources", "faq.html#resources", null ],
      [ "Theme the VMEMapView", "faq.html#theming", null ],
      [ "Integrating location services", "faq.html#updateLocation", null ]
    ] ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_v_m_e_compute_route_interface_8h.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';