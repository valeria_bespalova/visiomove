//
//  OverlayViewController.h
//  MapViewSample
//
//  Created by Jon Bryant on 10/06/2016.
//  Copyright © 2016 Visioglobe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VisioMoveEssential/VisioMoveEssential.h>

@interface OverlayViewController : UIViewController <VMEMapListener>
@property (strong, nonatomic) IBOutlet UILabel *infoViewLabel;
@property (strong, nonatomic) IBOutlet UISwitch *infoViewAddedSwitch;
@property (strong) IBOutlet VMEMapView* mapView;
@property (strong, nonatomic) IBOutlet UIButton *moveOverlayButton;

- (IBAction)infoViewSwitch:(id)sender;
- (IBAction)moveOverlay:(id)sender;

@end
