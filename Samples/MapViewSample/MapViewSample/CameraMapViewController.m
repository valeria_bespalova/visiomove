/*
 * Copyright 2016, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */


#import "CameraMapViewController.h"


@implementation CameraMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = NSLocalizedString(@"CameraMapViewController.title", nil);
    self.enableAnimationLabel.text = NSLocalizedString(@"CameraMapViewController.animate", nil);
    
    [self.floorButton setTitle:NSLocalizedString(@"CameraMapViewController.floor", nil) forState:UIControlStateNormal];
    [self.globalButton setTitle: NSLocalizedString(@"CameraMapViewController.global", nil) forState:UIControlStateNormal];
    [self.placeButton setTitle: NSLocalizedString(@"CameraMapViewController.place", nil) forState:UIControlStateNormal];
    [self.positionButton setTitle: NSLocalizedString(@"CameraMapViewController.position", nil) forState:UIControlStateNormal];
    
    [self.mapView setMapListener:self];
    [self.mapView loadMap];
    
    // IMPORTANT: Ensure the mapView starts below the navigation bar, otherwise
    // VisioMove Essential's search view button will be hidden.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}


-(IBAction)goToPlace:(id)sender
{
    VMECameraUpdate* lUpdate = [VMECameraUpdate cameraUpdateForPlaceID:@"0006"];
    BOOL lIsAnimated = [self.enableAnimationSwitch  isOn];
    (lIsAnimated) ? [self.mapView  animateCamera:lUpdate] : [self.mapView  updateCamera:lUpdate];
}

-(IBAction)goToFloor:(id)sender
{
    VMECameraUpdate* lUpdate = [VMECameraUpdate cameraUpdateForViewMode:VMEViewModeFloor
                                                                heading:[VMECameraHeading cameraHeadingCurrent]
                                                                floorID:FLOOR_1];
    BOOL lIsAnimated = [self.enableAnimationSwitch  isOn];
    (lIsAnimated) ? [self.mapView  animateCamera:lUpdate] : [self.mapView  updateCamera:lUpdate];
}

-(IBAction)goToGlobalMode:(id)sender
{
    VMECameraUpdate* lUpdate = [VMECameraUpdate cameraUpdateForViewMode:VMEViewModeGlobal
                                                                heading:[VMECameraHeading cameraHeadingCurrent]];
    BOOL lIsAnimated = [self.enableAnimationSwitch  isOn];
    (lIsAnimated) ? [self.mapView  animateCamera:lUpdate] : [self.mapView  updateCamera:lUpdate];
}

- (IBAction)gotoPosition:(id)sender {
    CLLocationCoordinate2D coordinate = TEST_COORDINATE;
    
    VMEPosition* lPos1 = [[VMEPosition alloc] initWithLatitude:coordinate.latitude
                                                     longitude:coordinate.longitude
                                                      altitude:1
                                                    buildingID:BUILDING
                                                       floorID:FLOOR_1];
    coordinate = TEST_COORDINATE_PLUS_DELTA;
    VMEPosition* lPos2 = [[VMEPosition alloc] initWithLatitude:coordinate.latitude
                                                     longitude:coordinate.longitude
                                                      altitude:1
                                                    buildingID:BUILDING
                                                       floorID:FLOOR_1];
    coordinate = TEST_COORDINATE_MINUS_DELTA;
    VMEPosition* lPos3 = [[VMEPosition alloc] initWithLatitude:coordinate.latitude
                                                     longitude:coordinate.longitude
                                                      altitude:1
                                                    buildingID:BUILDING
                                                       floorID:FLOOR_1];
    
    NSArray* lPositions = [[NSArray alloc] initWithObjects:lPos1, lPos2, lPos3, nil];
    
    VMECameraUpdate* lUpdate = [VMECameraUpdate cameraUpdateForPositions:lPositions
                                                                 heading:[VMECameraHeading cameraHeadingCurrent]
                                                              paddingTop:50
                                                           paddingBottom:50
                                                             paddingLeft:50
                                                            paddingRight:50];
                                
                                
    BOOL lIsAnimated = [self.enableAnimationSwitch  isOn];
    (lIsAnimated) ? [self.mapView  animateCamera:lUpdate] : [self.mapView  updateCamera:lUpdate];
}

@end
