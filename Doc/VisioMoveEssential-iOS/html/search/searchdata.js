var indexSectionsWithContent =
{
  0: "abcdfgilmpqrsuv",
  1: "v",
  2: "cfrv",
  3: "acgilmpqrsu",
  4: "v",
  5: "v",
  6: "abcdfilmprs",
  7: "v",
  8: "cdfg"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Macros",
  8: "Pages"
};

