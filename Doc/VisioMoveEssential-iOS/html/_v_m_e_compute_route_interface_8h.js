var _v_m_e_compute_route_interface_8h =
[
    [ "VMERouteRequest", "interface_v_m_e_route_request.html", "interface_v_m_e_route_request" ],
    [ "VMERouteResult", "interface_v_m_e_route_result.html", "interface_v_m_e_route_result" ],
    [ "<VMEComputeRouteCallback>", "protocol_v_m_e_compute_route_callback-p.html", "protocol_v_m_e_compute_route_callback-p" ],
    [ "<VMEComputeRouteInterface>", "protocol_v_m_e_compute_route_interface-p.html", "protocol_v_m_e_compute_route_interface-p" ],
    [ "VMERouteDestinationsOrder", "_v_m_e_compute_route_interface_8h.html#a5625ec9028ab6513869bec106b14cb82", [
      [ "VMERouteDestinationsOrderInOrder", "_v_m_e_compute_route_interface_8h.html#a5625ec9028ab6513869bec106b14cb82af2c0dc91bc01e6b70ede03a2dadb79f9", null ],
      [ "VMERouteDestinationsOrderOptimal", "_v_m_e_compute_route_interface_8h.html#a5625ec9028ab6513869bec106b14cb82abae1b40160983888d43f331797ac07f6", null ],
      [ "VMERouteDestinationsOrderOptimalFinishOnLast", "_v_m_e_compute_route_interface_8h.html#a5625ec9028ab6513869bec106b14cb82ab11c90bf64e5720cd7dd61889a334802", null ],
      [ "VMERouteDestinationsOrderClosest", "_v_m_e_compute_route_interface_8h.html#a5625ec9028ab6513869bec106b14cb82a17b2c4f965fc035b01c6d27689699c74", null ]
    ] ],
    [ "VMERouteRequestType", "_v_m_e_compute_route_interface_8h.html#a350a21313c3c121da156bdf5030c5438", [
      [ "VMERouteRequestTypeShortest", "_v_m_e_compute_route_interface_8h.html#a350a21313c3c121da156bdf5030c5438ae7a917577eca60aa724485df14f48cc9", null ],
      [ "VMERouteRequestTypeFastest", "_v_m_e_compute_route_interface_8h.html#a350a21313c3c121da156bdf5030c5438a65c112551edbde90aec1b07379caf51d", null ]
    ] ]
];